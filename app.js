var db = firebase.firestore();

function guardarCorreo(value_correo){
    return db.collection("correos").add({
        correo: value_correo
    })
}

const input_email = document.getElementById("input-email");

const button_email = document.getElementById("button-email");

button_email.addEventListener("click", function(){
    let correo = input_email.value;
    guardarCorreo(correo).then(function(docRef){
        console.log('Se puedo agregar el elemento a la base de datos - ' + docRef.id);
        input_email.value = "";
    }).catch(function(error){
        console.log(error);
    })
})
